<?php $__env->startPush('css'); ?>
    <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Hind&display=swap" rel="stylesheet">
    <!-- //web fonts -->
    <!-- Template CSS -->
    <link rel="stylesheet" href="/assets/css/style-liberty.css">
    <style>
        .w3l-blog-breadcrum .breadcrum-bg {
            background-image: linear-gradient(to right, rgba(42, 42, 42, 0.71), rgba(38, 40, 40, 0.78)), url(<?php echo e($property->getPhoto()); ?>);
        }
        .new-posts a {
            margin: 20px 0;
            grid-template-columns: auto auto;
            grid-gap: 10px;
            color: var(--title-color);
            font-weight: 600;
            align-items: center;
            display: none !important;
        }
        * {
            box-sizing: border-box;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;
        }


        #w3lDemoBar.w3l-demo-bar {
            top: 0;
            right: 0;
            bottom: 0;
            z-index: 9999;
            padding: 40px 5px;
            padding-top:70px;
            margin-bottom: 70px;
            background: #0D1326;
            border-top-left-radius: 9px;
            border-bottom-left-radius: 9px;
        }

        #w3lDemoBar.w3l-demo-bar a {
            display: block;
            color: #e6ebff;
            text-decoration: none;
            line-height: 24px;
            opacity: .6;
            margin-bottom: 20px;
            text-align: center;
        }

        #w3lDemoBar.w3l-demo-bar span.w3l-icon {
            display: block;
        }

        #w3lDemoBar.w3l-demo-bar a:hover {
            opacity: 1;
        }

        #w3lDemoBar.w3l-demo-bar .w3l-icon svg {
            color: #e6ebff;
        }
        #w3lDemoBar.w3l-demo-bar .responsive-icons {
            margin-top: 30px;
            border-top: 1px solid #41414d;
            padding-top: 40px;
        }
        #w3lDemoBar.w3l-demo-bar .demo-btns {
            border-top: 1px solid #41414d;
            padding-top: 30px;
        }
        #w3lDemoBar.w3l-demo-bar .responsive-icons a span.fa {
            font-size: 26px;
        }
        #w3lDemoBar.w3l-demo-bar .no-margin-bottom{
            margin-bottom:0;
        }
        .toggle-right-sidebar span {
            background: #0D1326;
            width: 50px;
            height: 50px;
            line-height: 50px;
            text-align: center;
            color: #e6ebff;
            border-radius: 50px;
            font-size: 26px;
            cursor: pointer;
            opacity: .5;
        }
        .pull-right {
            float: right;
            position: fixed;
            right: 0px;
            top: 70px;
            width: 90px;
            z-index: 99999;
            text-align: center;
        }
        /* ============================================================
        RIGHT SIDEBAR SECTION
        ============================================================ */

        #right-sidebar {
            width: 90px;
            position: fixed;
            height: 100%;
            z-index: 1000;
            right: 0px;
            top: 0;
            margin-top: 60px;
            -webkit-transition: all .5s ease-in-out;
            -moz-transition: all .5s ease-in-out;
            -o-transition: all .5s ease-in-out;
            transition: all .5s ease-in-out;
            overflow-y: auto;
        }


        /* ============================================================
        RIGHT SIDEBAR TOGGLE SECTION
        ============================================================ */

        .hide-right-bar-notifications {
            margin-right: -300px !important;
            -webkit-transition: all .3s ease-in-out;
            -moz-transition: all .3s ease-in-out;
            -o-transition: all .3s ease-in-out;
            transition: all .3s ease-in-out;
        }



        @media (max-width: 992px) {
            #w3lDemoBar.w3l-demo-bar a.desktop-mode{
                display: none;

            }
        }
        @media (max-width: 767px) {
            #w3lDemoBar.w3l-demo-bar a.tablet-mode{
                display: none;

            }
        }
        @media (max-width: 568px) {
            #w3lDemoBar.w3l-demo-bar a.mobile-mode{
                display: none;
            }
            #w3lDemoBar.w3l-demo-bar .responsive-icons {
                margin-top: 0px;
                border-top: none;
                padding-top: 0px;
            }
            #right-sidebar,.pull-right {
                width: 90px;
            }
            #w3lDemoBar.w3l-demo-bar .no-margin-bottom-mobile{
                margin-bottom: 0;
            }
        }
    </style>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?>

    <section class="w3l-blog-breadcrum">
        <div class="breadcrum-bg py-sm-5 py-4">
            <div class="container py-lg-3">
                <h2><?php echo e($property->title); ?></h2>
                <p><?php echo e(\Carbon\Carbon::parse($property->created_at)->format('d m Y')); ?></p>
            </div>
        </div>
    </section>
    <section class="text-11">
        <div class="text11 py-5">
            <div class="container py-md-3">
                <div class="row">
                    <div class="col-lg-8 text11-content">
                        <img src="<?php echo e($property->getPhoto()); ?>" class="img-fluid" alt="">
                        <p class="mt-4 mb-3">
                            <?php echo e($property->description); ?>

                        </p>
                    </div>
                    <div class="sidebar-side col-lg-4 col-md-12 col-sm-12 mt-lg-0 mt-5">
                        <aside class="sidebar">

                            <!-- Search -->
                            <div class="sidebar-widget search-box">
                                <div class="sidebar-title">
                                    <h4>Property Search</h4>
                                </div>
                                <form method="post" action="contact.html">
                                    <div class="form-group">
                                        <input type="search" name="search-field" value="" placeholder="Enter Location,Property.." required="">
                                        <button type="submit"><span class="icon fa fa-search"></span></button>
                                    </div>
                                </form>
                            </div>

                            <!--Blog Category Widget-->
                            <div class="sidebar-widget sidebar-blog-category">
                                <div class="sidebar-title">
                                    <h4>Categories</h4>
                                </div>
                                <ul class="blog-cat">
                                    <li><a href="#url">Marketing process <span>1</span></a></li>
                                    <li><a href="#url">Business partnership <span>5</span></a></li>
                                    <li><a href="#url">Development <span>8</span></a></li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/mustafa/Desktop/personal/alligatedubai/resources/views/pages/detail.blade.php ENDPATH**/ ?>