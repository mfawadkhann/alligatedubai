<!-- Top Menu 1 -->
<section class="w3l-top-menu-1">
    <div class="top-hd">
        <div class="container">
            <header class="row">
                <div class="social-top col-lg-3 col-6">
                    <li>Follow Us</li>
                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                    <li><a href="#"><span class="fa fa-instagram"></span></a> </li>
                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fa fa-vimeo"></span></a> </li>
                </div>
                <div class="accounts col-lg-9 col-6">
                    <li class="top_li"><span class="fa fa-mobile"></span><a href="tel:+142 5897555">+971 55 549 1418</a> </li>
                    <li class="top_li1"><a href="#">Login</a></li>
                    <li class="top_li2"><a href="#">Register</a></li>
                </div>

            </header>
        </div>
    </div>
</section>
<!-- //Top Menu 1 -->

<section class="w3l-bootstrap-header">
    <nav class="navbar navbar-expand-lg navbar-light py-lg-2 py-2">
        <div class="container">
            <a class="navbar-brand" href="/"><span class="fa fa-home"></span> Estate Agent</a>
            <!-- if logo is image enable this
<a class="navbar-brand" href="#index.html">
    <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
</a> -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon fa fa-bars"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo e(route('home')); ?>">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo e(route('about')); ?>">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo e(route('services')); ?>">Services</a>
                        </li>

                        <li class="nav-item mr-0">
                            <a class="nav-link" href="<?php echo e(route('contact')); ?>">Contact</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    </section>
<?php /**PATH /home/mustafa/Desktop/personal/alligatedubai/resources/views/includes/header.blade.php ENDPATH**/ ?>